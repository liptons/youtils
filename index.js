/**
 * @typedef YouTubeVideo
 * @property {string} title
 * @property {string} channel
 * @property {string} description
 * @property {string} image
 * @property {string[]} tags
 */

/**
* @typedef SearchResult
* @property {string} title
* @property {string} id
*/

/**
 * @function Search
 * @argument string Searches youtube for a string and grabs all the results on the first page
 * @returns {Promise}
 * @resolves {SearchResult[]} a promise that resolves with an array of SearchResult objects
 */
const search = require('./search.js')

/**
 * @function getVideoInfo
 * @argument id Youtube video id
 * @returns {Promise}
 * @resolves {YouTubeVideo} a promise that resolves with a YouTubeVideo object
 */
const getVideoInfo = require('./getVideoInfo.js')

const getChannelInfo = require('./getChannelInfo.js')

module.exports = { search, getVideoInfo, getChannelInfo }