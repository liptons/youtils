describe("getVideoInfo", function() {
    const getVideoInfo = require("../getVideoInfo.js")
	const expected = {
		id: "j091EzG2b8s",
		title: "YouTube Rewind 2019 failed and its my fault!",
		channel: "UC-lHJZR3Gqxm24_Vd_AJ5Yw",
		// channel: "UCi-g4cjqGV7jvU8aeSuj0jQ",
		description:
			"Why did youtube rewind 2019 flop? #rewind2019 fail epicly 100 CLUB MERCH OUT NOW! https://represent.com/store/pewdiepie (Thank you) Pewdiepie's Pixelings DOW...",
		image: "https://i.ytimg.com/vi/j091EzG2b8s/maxresdefault.jpg",
		tags: [
			"SATIRE",
			"youtube rewind 2019",
			"youtube rewind",
			"youtube rewind 2018",
			"youtube rewind but it's actually good",
			"youtube rewind good",
			"youtube rewind bad",
			"youtube rewind cringe",
			"youtube rewind pewdiepie",
			"azzyland",
			"will smith",
			"yt rewind",
			"youtube",
			"rewind",
			"youtube rewind 2019 pewdiepie",
			"youtube rewind 2019 meme",
			"youtube rewind 2019 pewdiepie reaction",
			"youtube rewind 2019 trailer"
		]
	}

	it(`should return meta data for PewDiePie's video titled "${expected.title}"`, async function() {
		var info = await getVideoInfo(expected.id)
		expect(info.id).toEqual(expected.id)
		expect(info.title).toEqual(expected.title)
		expect(info.channel).toEqual(expected.channel)
		expect(info.image).toEqual(expected.image)
		expect(info.tags).toEqual(expected.tags)
		expect(info.description).toEqual(expected.description)
	})
})
