
describe("A suite is just a function", function() {
    const search = require("./../search.js")
    
    
	it("should return multiple valid search results objects", async function() {

        var results = await search("cat")

        expect(results.length).toBeGreaterThan(1)

        results.forEach(result => {
            expect(result.id).toBeInstanceOf(String)
            expect(result.id.length).toEqual(11)
            expect(result.title).toBeInstanceOf(String)
            expect(result.title.length).toBeGreaterThan(1)
        })
        
    })

})
