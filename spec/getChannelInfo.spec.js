describe("getChannelInfo", function() {
    const getChannelInfo = require("../getChannelInfo.js")
	const expected = {
		id: "UC-lHJZR3Gqxm24_Vd_AJ5Yw",
		title: "PewDiePie"
	}

	it(`should return meta data for PewDiePie's video titled "${expected.title}"`, async function() {
        var info = await getChannelInfo(expected.id)
		expect(info.id).toEqual(expected.id)
		expect(info.title).toEqual(expected.title)
	})
})
