const cherio = require('cherio')
const fetch = require('./fetch.js')

function getVideoTitle(element) {
    return element.text()
}

function getVideoId(element) {
    return element.attr('href')
        .split('?v=')[1]
}

module.exports = async function search(string) {

    var $ = await fetch(`https://www.youtube.com/results?search_query=${encodeURIComponent(string)}`)

    var videos = []

    $("a.yt-uix-tile-link")
        .each(function (i, element) {

            element = $(element)

            var video = {
                title: getVideoTitle(element),
                id: getVideoId(element)
            }

            if (video.id && video.title) {
                videos.push(video)
            }

        })

    return videos

}