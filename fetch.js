const request = require('request')
const cherio = require('cherio')
module.exports = (url) => {
    return new Promise((resolve, reject) => {
        request(url, function (err, response, body) {
            if (err) return reject(err)
            var out = cherio.load(body)
            resolve(out)
        })
    })
}