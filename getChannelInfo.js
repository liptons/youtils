const fetch = require('./fetch.js')

module.exports = async function (channelId) {

    var $ = await fetch(`https://www.youtube.com/channel/${channelId}`)

    var info = {
        id: channelId,
        title: '',
    }

    // console.log($.xml())
    // const fs = require('fs')
    // fs.writeFile('./poop.html', $.xml(), () => {})

    $('meta').each((tag, element) => {
        element = $(element)

        var name = element.attr('name'),
            content = element.attr('content')

        if (name && content) {

            if (name === 'title') info.title = content

        }

    })


    return info
}