const fetch = require('./fetch.js')
module.exports = async function VideoInfo(videoId) {

    var $ = await fetch(`https://www.youtube.com/watch?v=${videoId}`),
        info = {
            id: videoId,
            title: '',
            channel: '',
            description: '',
            image: '',
            tags: []
        }
        // const fs = require('fs')
        // fs.writeFile('./poop.html', $.xml(), () => {})
    $('a.spf-link').each((tag, element) => {
        element = $(element)
        var href = element.attr('href') || ''
        if (~href.indexOf('/channel/')) {
            info.channel = href.replace('/channel/', '')
        }
    })

    $('meta').each((tag, element) => {

        element = $(element)

        var property = element.attr('property'),
            itemProp = element.attr('itemprop'),
            content = element.attr('content'),
            name = element.attr('name')

        if (name === 'title') {
            // content = video title
            info.title = content
        }

        if (itemProp === 'channelId') {
            // content = channel id
            info.channel = content
        }

        if (property === 'og:video:tag') {
            // content = video tag
            info.tags.push(content)
        }

        if (property === 'og:description') {
            // content = video description
            info.description = content
        }

        if (property === 'og:image') {
            // content = video thumbnail
            info.image = content
        }

    })

    return info

}